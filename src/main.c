/*==================[inclusions]=============================================*/

#include "main.h"

#include "board.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/* Include statechart header file. Be sure you run the statechart C code
 * generation tool!
 */
#include "Ejemplo_cintas.h"

/*==================[macros and definitions]=================================*/

#define STACK_SIZE 1024

#define PUERTO_IO  0
#define PUERTO_VEL 2

#define VELOCIDAD1 0x0001
#define VELOCIDAD2 0x0002

/* tick en 10ms! */
#define TIEMPO_CINTA1 3000
#define TIEMPO_CINTA2 4000

#define PIN_D1 0
#define PIN_D2 1
#define PIN_F1 2
#define PIN_F2 3
#define PIN_S1 4
#define PIN_S2 5
#define PIN_OC 6

typedef struct {
	Ejemplo_cintas statechart;
	xSemaphoreHandle semSensorInicio;
	xSemaphoreHandle semPosicionColector;
	xSemaphoreHandle semTick;
	uint32_t tiempoCinta;
}taskParam_t;

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/** @brief hardware initialization function
 *	@return none
 */
static void initHardware(void);

/*==================[internal data definition]===============================*/

static taskParam_t param_t1;
static taskParam_t param_t2;

xSemaphoreHandle mutexCintas;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/** clock and peripherals initialization */
static void initHardware(void)
{
	Board_Init();
	SystemCoreClockUpdate();

	/* configuro pines de entrada por irq, flanco descendente */
    Chip_GPIOINT_Init(LPC_GPIOINT);
    Chip_GPIOINT_SetIntFalling(LPC_GPIOINT, GPIOINT_PORT0,
    		(1 << PIN_D1) | (1 << PIN_D2) | (1 << PIN_S1) | (1 << PIN_S2));
    NVIC_EnableIRQ(EINT3_IRQn);
}

static void tareaCinta(void * p)
{
	taskParam_t * t = (taskParam_t *)p;
	xSemaphoreHandle s;
	xQueueSetHandle set;

	set = xQueueCreateSet(3);

	xQueueAddToSet(t->semPosicionColector, set);
	xQueueAddToSet(t->semSensorInicio, set);
	xQueueAddToSet(t->semTick, set);

	ejemplo_cintas_init(&(t->statechart));
	ejemplo_cintas_enter(&(t->statechart));

	ejemplo_cintasIface_set_veTiempoCinta(&(t->statechart), t->tiempoCinta);

	while (1) {

		s = xQueueSelectFromSet(set, portMAX_DELAY);

		if (s == t->semPosicionColector) {
			ejemplo_cintasIface_raise_evColectorEnPosicion(&(t->statechart));
		}
		if (s == t->semSensorInicio) {
			xSemaphoreTake(mutexCintas, portMAX_DELAY);
			ejemplo_cintasIface_raise_evSensorInicio(&(t->statechart));
		}
		if (s == t->semTick) {
			ejemplo_cintasIface_raise_evTick(&(t->statechart));
		}

		ejemplo_cintas_runCycle(&(t->statechart));

		/* resetear el semaforo que fue liberado para que select vuelva a bloquear */
		xSemaphoreTake(s, 0);
	}

}

/*==================[external functions definition]==========================*/

void ejemplo_cintasIface_opFrenarCinta(const Ejemplo_cintas* handle, const sc_boolean freno)
{
	if (handle == &(param_t1.statechart)) {
		if (freno == false) {
			Chip_GPIO_SetPortValue(LPC_GPIO, PUERTO_VEL, VELOCIDAD1);
		}
		Chip_GPIO_SetPinState(LPC_GPIO, PUERTO_IO, PIN_F1, freno);
	}
	if (handle == &(param_t2.statechart)) {
		if (freno == false) {
			Chip_GPIO_SetPortValue(LPC_GPIO, PUERTO_VEL, VELOCIDAD2);
		}
		Chip_GPIO_SetPinState(LPC_GPIO, PUERTO_IO, PIN_F2, freno);
	}
	if (freno == true) {
		xSemaphoreGive(mutexCintas);
	}
}

void ejemplo_cintasIface_opMoverColector(const Ejemplo_cintas* handle, const sc_boolean mover)
{
	Chip_GPIO_SetPinState(LPC_GPIO, PUERTO_IO, PIN_OC, mover);
}


/** @brief 10ms tickhook
 *  @warning this is called by an interrupt handler!
 */
void vApplicationTickHook(void)
{
	ejemplo_cintasIface_raise_evTick(&(param_t1.statechart));
	ejemplo_cintasIface_raise_evTick(&(param_t2.statechart));
}

/** @brief main function, application entry point */
int main(void)
{
	mutexCintas = xSemaphoreCreateMutex();
	vSemaphoreCreateBinary(param_t1.semPosicionColector);
	vSemaphoreCreateBinary(param_t1.semSensorInicio);
	vSemaphoreCreateBinary(param_t2.semPosicionColector);
	vSemaphoreCreateBinary(param_t2.semSensorInicio);

	param_t1.tiempoCinta = TIEMPO_CINTA1;
	param_t2.tiempoCinta = TIEMPO_CINTA2;

	/* enable irq */
	initHardware();

	xTaskCreate(tareaCinta, (const signed char * const)"cinta1", STACK_SIZE, &param_t1, tskIDLE_PRIORITY+1, 0);
	xTaskCreate(tareaCinta, (const signed char * const)"cinta2", STACK_SIZE, &param_t2, tskIDLE_PRIORITY+1, 0);

	vTaskStartScheduler();

	while (1) {

	}
}

/** @brief interrupción externa de GPIO en el puerto 0 */
void EINT3_IRQHandler(void)
{
	static portBASE_TYPE xSwitchRequired;

	if (Chip_GPIOINT_GetStatusFalling(LPC_GPIOINT, GPIOINT_PORT0) & (1<<PIN_D1)) {
		Chip_GPIOINT_ClearIntStatus(LPC_GPIOINT, GPIOINT_PORT0, 1<<PIN_D1);
		xSemaphoreGiveFromISR(param_t1.semSensorInicio, &xSwitchRequired);
	}
	if (Chip_GPIOINT_GetStatusFalling(LPC_GPIOINT, GPIOINT_PORT0) & (1<<PIN_D2)) {
		Chip_GPIOINT_ClearIntStatus(LPC_GPIOINT, GPIOINT_PORT0, 1<<PIN_D2);
		xSemaphoreGiveFromISR(param_t2.semSensorInicio, &xSwitchRequired);
	}
	if (Chip_GPIOINT_GetStatusFalling(LPC_GPIOINT, GPIOINT_PORT0) & (1<<PIN_S1)) {
		Chip_GPIOINT_ClearIntStatus(LPC_GPIOINT, GPIOINT_PORT0, 1<<PIN_S1);
		xSemaphoreGiveFromISR(param_t1.semPosicionColector, &xSwitchRequired);
	}
	if (Chip_GPIOINT_GetStatusFalling(LPC_GPIOINT, GPIOINT_PORT0) & (1<<PIN_S2)) {
		Chip_GPIOINT_ClearIntStatus(LPC_GPIOINT, GPIOINT_PORT0, 1<<PIN_S2);
		xSemaphoreGiveFromISR(param_t2.semPosicionColector, &xSwitchRequired);
	}
	portEND_SWITCHING_ISR(xSwitchRequired);
}

/*==================[end of file]============================================*/
